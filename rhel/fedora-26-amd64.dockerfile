FROM fedora:26
MAINTAINER Nanyc Team

RUN dnf group install -y "C Development Tools and Libraries" \
    && dnf install -y \
        cmake \
        unzip \
    && dnf clean all -y
