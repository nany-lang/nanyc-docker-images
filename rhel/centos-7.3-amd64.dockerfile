FROM centos:7.3.1611
MAINTAINER Nanyc Team

RUN yum install -y centos-release-scl-rh \
    && INSTALL_PKGS="devtoolset-6-gcc devtoolset-6-gcc-c++ devtoolset-6-gcc-gfortran devtoolset-6-gdb" \
	&& EXTRA_PKGS="coreutils curl gzip make redhat-rpm-config rpm-build tar unzip wget" \
    && yum install -y --setopt=tsflags=nodocs $INSTALL_PKGS \
	&& yum install -y --setopt=tsflags=nodocs $EXTRA_PKGS \
    && rpm -V $INSTALL_PKGS $EXTRA_PKGS\
    && yum clean all -y

ENV HOME=/opt/app-root/src \
    PATH=/opt/app-root/src/bin:/opt/app-root/bin:/opt/rh/devtoolset-6/root/usr/bin/:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

WORKDIR ${HOME}

# Enable the SCL for all bash scripts.
ENV BASH_ENV=/opt/app-root/etc/scl_enable \
    ENV=/opt/app-root/etc/scl_enable \
    PROMPT_COMMAND=". /opt/app-root/etc/scl_enable"

RUN gcc --version && g++ --version

RUN export CMAKE_VERSION=3.9.3 \
    && wget --quiet "https://cmake.org/files/v3.9/cmake-${CMAKE_VERSION}.tar.gz" \
    && tar zxf cmake-${CMAKE_VERSION}.tar.gz \
        && cd cmake-${CMAKE_VERSION}/ && ./configure && make -j$(nproc) \
        && ./bin/cpack -G RPM \
        && yum localinstall -y cmake-*.rpm \
        && cd .. && rm -rf cmake-*
