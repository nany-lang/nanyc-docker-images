#!/usr/bin/env bash

. "_utils.sh"

push_docker_image() {
	local folder="$1"
	local name="$2"
	local version="$3"
	local arch="$4"
	echo -e "\n::: push ${name}:${version}-${arch}..."
	docker push "${repository}/build/${name}:${version}-${arch}"
}

each push_docker_image
