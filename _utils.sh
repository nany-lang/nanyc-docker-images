set -e
root=$(cd $(dirname "$0") && pwd)
images=()
repository="registry.gitlab.com/nany-lang/nanyc"

each() {
	$1 debian debian 8     amd64
	$1 debian debian 9     amd64
	$1 debian ubuntu 16.04 amd64
	$1 debian ubuntu 17.04 amd64
	$1 debian ubuntu 18.04 amd64
	$1 rhel   centos 7.3   amd64
	$1 rhel   centos 7.4   amd64
	$1 rhel   fedora 26    amd64
}
