FROM ubuntu:18.04
MAINTAINER Nanyc Team

RUN apt-get update && apt-get --yes upgrade && apt-get --yes install \
        apt-utils \
        build-essential \
        cmake \
        unzip \
   && rm -rf /var/lib/apt/lists/*
