FROM debian:stretch-slim
MAINTAINER Nanyc Team

RUN apt-get update && apt-get --yes upgrade && apt-get --yes install \
        apt-utils \
        build-essential \
        cmake \
        unzip \
   && rm -rf /var/lib/apt/lists/*
