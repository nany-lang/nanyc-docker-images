FROM ubuntu:16.04
MAINTAINER Nanyc Team

RUN apt-get update && apt-get --yes upgrade && apt-get --yes install \
        apt-utils \
        build-essential \
        unzip \
        wget \
   && rm -rf /var/lib/apt/lists/*

ENV CMAKE_VERSION 3.9.3
RUN wget --quiet "https://cmake.org/files/v3.9/cmake-${CMAKE_VERSION}.tar.gz" \
    && tar zxf cmake-${CMAKE_VERSION}.tar.gz \
        && cd cmake-${CMAKE_VERSION}/ && ./configure && make -j$(nproc) && ./bin/cpack -G DEB \
        && dpkg -i cmake-*.deb \
        && cd .. && rm -rf cmake-*
