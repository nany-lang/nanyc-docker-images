#!/usr/bin/env bash

. "_utils.sh"

build_docker_image() {
	local folder="$1"
	local name="$2"
	local version="$3"
	local arch="$4"
	local file="${name}-${version}-${arch}.dockerfile"
	echo -e "\n::: building ${name}:${version}-${arch}..."
	(cd "${folder}" \
		&& docker build --squash --pull=true -f "./${file}" -t "${repository}/build/${name}:${version}-${arch}" .)
}

each build_docker_image
